/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.lab3;

/**
 *
 * @author ACER
 */
public class XOGame {

    static boolean checkWin(char[][] Table, char currentPlayer) {
        if (checkRow(Table, currentPlayer) || checkCol(Table, currentPlayer) || checkX1(Table, currentPlayer) || checkX2(Table, currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(char[][] Table, char currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(Table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(char[][] Table, char currentPlayer, int row) {
        for (int i = 0; i < 3; i++) {
            if (Table[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] Table, char currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(Table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] Table, char currentPlayer, int col) {
        for (int j = 0; j < 3; j++) {
            if (Table[j][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1(char[][] Table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (Table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(char[][] Table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (Table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDraw(char[][] Table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] == '-') {
                    return false;
                }
            }

        }
        return true;
    }

}
