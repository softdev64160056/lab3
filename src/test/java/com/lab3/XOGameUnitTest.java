/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ACER
 */
public class XOGameUnitTest {
    
    public XOGameUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test 
    public void testCheckWinNoPlay_O_output_False(){
        char[][] Table = {{'-', '-', '-',}, {'-', '-', '-',}, {'-', '-', '-',}};
        char currentPlayer = 'O';
        assertEquals(false, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckRow1_O_output_True(){
        char[][] Table = {{'O', 'O', 'O',}, {'-', '-', '-',}, {'-', '-', '-',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckRow2_O_output_True(){
        char[][] Table = {{'-', '-', '-',}, {'O', 'O', 'O',}, {'-', '-', '-',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckRow3_O_output_True(){
        char[][] Table = {{'-', '-', '-',}, {'-', '-', '-',}, {'O', 'O', 'O',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    
    @Test 
    public void testCheckRow1_X_output_True(){
        char[][] Table = {{'X', 'X', 'X',}, {'-', '-', '-',}, {'-', '-', '-',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckRow2_X_output_True(){
        char[][] Table = {{'-', '-', '-',}, {'X', 'X', 'X',}, {'-', '-', '-',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckRow3_X_output_True(){
        char[][] Table = {{'-', '-', '-',}, {'-', '-', '-',}, {'X', 'X', 'X',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckCol1_O_output_True(){
        char[][] Table = {{'O', '-', '-',}, {'O', '-', '-',}, {'O', '-', '-',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckCol2_O_output_True(){
        char[][] Table = {{'-', 'O', '-',}, {'-', 'O', '-',}, {'-', 'O', '-',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckCol3_O_output_True(){
        char[][] Table = {{'-', '-', 'O',}, {'-', '-', 'O',}, {'-', '-', 'O',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckCol1_X_output_True(){
        char[][] Table = {{'X', '-', '-',}, {'X', '-', '-',}, {'X', '-', '-',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckCol2_X_output_True(){
        char[][] Table = {{'-', 'X', '-',}, {'-', 'X', '-',}, {'-', 'X', '-',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckCol3_X_output_True(){
        char[][] Table = {{'-', '-', 'X',}, {'-', '-', 'X',}, {'-', '-', 'X',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckX1_O_output_True(){
        char[][] Table = {{'O', '-', '-',}, {'-', 'O', '-',}, {'-', '-', 'O',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckX1_X_output_True(){
        char[][] Table = {{'X', '-', '-',}, {'-', 'X', '-',}, {'-', '-', 'X',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckX2_O_output_True(){
        char[][] Table = {{'-', '-', 'O',}, {'-', 'O', '-',}, {'O', '-', '-',}};
        char currentPlayer = 'O';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckX2_X_output_True(){
        char[][] Table = {{'-', '-', 'X',}, {'-', 'X', '-',}, {'X', '-', '-',}};
        char currentPlayer = 'X';
        assertEquals(true, XOGame.checkWin(Table, currentPlayer));
    }
    @Test 
    public void testCheckDraw_O_output_True(){
        char[][] Table = {{'O', 'O', 'X',}, {'O', 'X', 'X',}, {'X', 'O', 'O',}};
        assertEquals(true, XOGame.checkDraw(Table));
    }
    @Test 
    public void testCheckDraw_X_output_True(){
        char[][] Table = {{'X', 'X', 'O',}, {'X', 'O', 'O',}, {'O', 'X', 'X',}};
        assertEquals(true, XOGame.checkDraw(Table));
    }
}
